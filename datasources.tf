######
# Informtation from Remote States
######

data "terraform_remote_state" "networking" {
  workspace = terraform.workspace
  backend   = "s3"

  config = {
    bucket = var.remote_state_bucket # Describes the S3 bucket where the state is stored
    region = var.remote_state_region # Describes the AWS region where the above bucket exists
    # Key selects the specific folder in S3 that contains the remote state you wish to read from
    key = var.region == "ap-southeast-2" ? "sandbox-networking/tfstate" : "sandbox-networking/${var.region}/tfstate"
  }
}

# Uncomment below for non-sandbox use-cases
#data "terraform_remote_state" "networking" {
#  workspace = terraform.workspace
#  backend   = "s3"
#
#  config = {
#    bucket = var.remote_state_bucket
#    region = var.remote_state_region
#    key    = var.region == "ap-southeast-2" ? "networking/tfstate" : "networking/${var.region}/tfstate"
#  }
#}

data "terraform_remote_state" "security_groups" {
  workspace = terraform.workspace
  backend   = "s3"

  config = {
    bucket = var.remote_state_bucket
    region = var.remote_state_region
    # Automatically use the region-appropriate remote state
    key = var.region == "ap-southeast-2" ? "security_groups/tfstate" : "security_groups/${var.region}/tfstate"
  }
}

## This is broken on purpose, pulls DNS data from either sandbox or prod.
# Include if you need DNS from sandbox environment.
data "terraform_remote_state" "dns" {
  workspace = terraform.workspace
  backend   = "s3"

  config = {
    bucket = var.remote_state_bucket
    region = var.remote_state_region
    key    = "sandbox-dns/tfstate"
  }
}

# Uncomment this if you need prod DNS
#data "terraform_remote_state" "dns" {
#  workspace = "${terraform.workspace}"
#  backend   = "s3"
#
#  config {
#    bucket = "${var.remote_state_bucket}"
#    region = "${var.remote_state_region}"
#    key    = "prod-dns/tfstate"
#  }
#}

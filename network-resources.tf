##########
# Private subnet - creating private CIDRs in a VPC
##########

# Set up by running:
# git clone git@gitlab.com:mnf-group/ecosystem/terraform/modules/private_subnet.git modules/private_subnet

# Uncomment below to use
//module "private_subnet" {
//  source                    = "../modules/private_subnet"
//  name                      = "${var.name}-private-subnet"
//  tags                      = { Owner = "brendand" }
//  private_subnets           = ["10.x.x.0/24","10.x.x.0/24","10.x.x.0/24"]
//  terraform_workspace_name  = "${terraform.workspace}"
//  region                    = "${var.region}"
//}


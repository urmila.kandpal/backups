###########
# Example Ec2 instance
##########

## "aws_instance" -> EC2
# https://www.terraform.io/docs/providers/aws/r/instance.html

# resource "aws_instance" "give proper name - this is an example" {
#   count                       = number of EC2 instances. One on each subnet. sourced from datasources.tf
#   ami                         = https://www.terraform.io/docs/providers/aws/r/ami.html. "${data.aws_ami.ubuntu.id}"
#   instance_type               = Instance type, "t2.micro" is free
#   associate_public_ip_address = Allocate a public IP for the EC2 (Boolean)
#   key_name                    = "${aws_key_pair.a_demo_keypair.key_name}"

#   subnet_id                   = "${data.terraform_remote_state.networking.private_subnets[count.index]}"
#                               See: https://gitlab.com/mnf-group/ecosystem/terraform/sandbox-networking/blob/master/output.tf

#   vpc_security_group_ids      = uses the security group, allow office http. 
#                          See: https://gitlab.com/mnf-group/ecosystem/terraform/security-groups/blob/master/output.tf 
#                               https://gitlab.com/mnf-group/ecosystem/terraform/security-groups/blob/master/resources.tf

## Creates tags for resources, count-index refers to count var above. Auto-incremented tag labelling.
# Merges data from local variables and environment variables.

#   volume_tags                 = "${merge(local.tags,
#                                    var.common_tags,
#                                    map("Name", "${var.env}-web-${count.index}"))}" 
#   tags                        = "${merge(local.tags,
#                                    var.common_tags, 
#                                    map("Name", "${var.env}-web-${count.index}"))}"

## Uses configure-server.sh in file/ dir to config/run ops on the EC2 once it runs.
#   user_data                   = "${file("configure-server.sh")}"
# }

data "aws_ami" "centos-ami" {
  owners      = ["self"]
  most_recent = true
  filter {
    name = "name" # Search by name, for value from config file

    values = [var.ami_name]
  }
}

resource "aws_instance" "skeleton-example-instance" {
  count = length(
    data.terraform_remote_state.networking.outputs.private_subnets,
  ) #give it a proper name - this is an example

  ami                         = data.aws_ami.centos-ami.id
  instance_type               = "t2.micro"
  associate_public_ip_address = false
  root_block_device {
    volume_type           = "standard"
    delete_on_termination = "true"
  }

  # Set up the instance in the appropriate private subnets
  subnet_id              = data.terraform_remote_state.networking.outputs.private_subnets[count.index]
  vpc_security_group_ids = [data.terraform_remote_state.security_groups.outputs.allow_office_public_http_id]

  volume_tags = merge(
    var.common_tags,
    var.tags,
    {
      Name = "${terraform.workspace}-web-${count.index}"
    },
  )

  tags = merge(
    var.common_tags,
    var.tags,
    {
      Name = "${terraform.workspace}-web-${count.index}"
    },
  )
  #user_data                   = "${file("configure-server.sh")}"
}

## How to create a Route 53 / DNS Record for your Instance.
# The record below doesn't make sense as it is assigning a DNS record to a private DNS
#this would create 3 DNS entries: skeleton-example-instance-[0/1/2].sandbox.ap2.mnfgroup.limited
resource "aws_route53_record" "skeleton-example" {
  count   = length(aws_instance.skeleton-example-instance)
  zone_id = data.terraform_remote_state.dns.outputs.sandbox_ap2_mnfgroup_limited_zone_id
  name    = "skeleton-example-instance-${count.index}"
  type    = "A"
  records = aws_instance.skeleton-example-instance.*.private_ip
  ttl     = "300"
}


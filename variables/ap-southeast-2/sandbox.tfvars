name = "sandbox"
region = "ap-southeast-2"
azs = ["ap-southeast-2a","ap-southeast-2b","ap-southeast-2c"]
instance_tags = { Owner = "MyNameIs" }
ami_name = "Centos 7.5 Base - Encrypted"
# AWS-K8S Sandbox Public AZ-a/b/c
private_subnets = ["10.221.46.0/23", "10.221.48.0/23", "10.221.50.0/23"]
private_subnet_suffix = "private"
name = "sandbox"
region = "ap-southeast-1"
azs = ["ap-southeast-1a","ap-southeast-1b","ap-southeast-1c"]
instance_tags = { Owner = "MyNameIs" }
ami_name = "Centos 7.5 Base - Encrypted"
# AWS-K8S Sandbox Public AZ-a/b/c
private_subnets = ["172.31.16.0/20", "172.31.32.0/20", "172.31.0.0/20"]
private_subnet_suffix = "private"
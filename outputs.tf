#### Terraform outputs

### Access these in another project using a data source

output "skeleton_instance_id" {
  description = "A list of the IDs of the instances created"
  value       = aws_instance.skeleton-example-instance.*.id
}
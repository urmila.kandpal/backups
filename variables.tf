variable "name" {
  default = "Skeleton"
}

variable "private_subnets" {
  description = "Subnets to create resources in, taken from the ./variables folder"
  type        = list(string)
}

variable "ami_name" {
  description = "The AMI to look-up for an EC2 instance"
  type        = string
}

variable "common_tags" {
  type        = map(string)
  description = "These tags will be appended to all resources created by terraform"
  default = {
    "mnfgroup:team"             = "platformsandnetworks"
    "mnfgroup:contact"          = "systems@mnfgroup.limited"
    "mnfgroup:project"          = "Skeleton"
    "mnfgroup:environment"      = "sandbox"
    "mnfgroup:tagging-version"  = "2.0"
    "mnfgroup:provisioner"      = "terraform"
    "mnfgroup:application-role" = "Skeleton"
  }
}

variable "tags" {
  description = "Tags from each environment's tfvars file"
  type        = "map"
  default     = {}
}

variable "region" {
  description = "The AWS region to create these resources in"
  default     = "ap-southeast-2"
}

variable "remote_state_bucket" {
  description = "Global remote state bucket, same for all projects, no matter the region"
  default     = "mnfgroup-terraform-remotestate"
}

variable "remote_state_region" {
  description = "Global remote state s3 bucket region, same for all projects, no matter the region"
  default     = "ap-southeast-2"
}
